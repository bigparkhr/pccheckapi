package com.bg.pccheckapi.controller;

import com.bg.pccheckapi.model.PcRoomItem;
import com.bg.pccheckapi.model.PcRoomRequest;
import com.bg.pccheckapi.model.PcRoomResponse;
import com.bg.pccheckapi.service.PcRoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pcRoom")
public class PcRoomController {
    private final PcRoomService pcRoomService;

    @PostMapping("/data")
    public String setPcRoom(@RequestBody PcRoomRequest request) {
        pcRoomService.setPcRoom(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PcRoomItem> getPcRooms() {return pcRoomService.getPcRoom();}

    @GetMapping("/detail/{id}")
    public PcRoomResponse getPcRoom(@PathVariable long id) {
        return pcRoomService.getPcRoom(id);
    }
}
