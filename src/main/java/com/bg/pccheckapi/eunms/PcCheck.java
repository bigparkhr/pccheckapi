package com.bg.pccheckapi.eunms;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcCheck {
    CHECK_OK_GOOD("좋음", true),
    CHECK_OK_BAD("나쁨", true),
    CHECK_NO("모름", false);

    private final String pcStatus;
    private final Boolean pcStatusCheck;

}
