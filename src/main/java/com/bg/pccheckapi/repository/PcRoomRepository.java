package com.bg.pccheckapi.repository;

import com.bg.pccheckapi.entity.PcRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcRoomRepository extends JpaRepository<PcRoom, Long> {
}
