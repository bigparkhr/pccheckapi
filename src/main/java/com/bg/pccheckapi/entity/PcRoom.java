package com.bg.pccheckapi.entity;

import com.bg.pccheckapi.eunms.FixType;
import com.bg.pccheckapi.eunms.PcCheck;
import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcRoom {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false)
    private Short computerNo;

    @Column(nullable = false, length = 10)
    private String update;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private FixType fixType;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PcCheck pcCheck;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;



}
