package com.bg.pccheckapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcRoomResponse {
    private Long id;
    private LocalDate dateCreate;
    private Short computerNo;
    private String update;
    private String fixTypeName;
    private String pcCheckName;

}
