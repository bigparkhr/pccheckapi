package com.bg.pccheckapi.model;

import com.bg.pccheckapi.eunms.FixType;
import com.bg.pccheckapi.eunms.PcCheck;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcRoomRequest {
    private Short computerNo;

    private String update;

    @Enumerated(value = EnumType.STRING)
    private FixType fixType;

    @Enumerated(value = EnumType.STRING)
    private PcCheck pcCheck;

    private String etcMemo;
}
