package com.bg.pccheckapi.model;

import com.bg.pccheckapi.eunms.FixType;
import com.bg.pccheckapi.eunms.PcCheck;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcRoomItem {
    private Long id;
    private LocalDate dateCreate;
    private Short computerNo;
    private String update;
    private String fixType;
    private String pcCheck;

}
