package com.bg.pccheckapi.service;

import com.bg.pccheckapi.entity.PcRoom;
import com.bg.pccheckapi.model.PcRoomItem;
import com.bg.pccheckapi.model.PcRoomRequest;
import com.bg.pccheckapi.model.PcRoomResponse;
import com.bg.pccheckapi.repository.PcRoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcRoomService {
    private final PcRoomRepository pcRoomRepository;

    public void setPcRoom(PcRoomRequest request) {
        PcRoom addData = new PcRoom();
        addData.setDateCreate(LocalDate.now());
        addData.setComputerNo(request.getComputerNo());
        addData.setUpdate(request.getUpdate());
        addData.setFixType(request.getFixType());
        addData.setPcCheck(request.getPcCheck());
        addData.setEtcMemo(request.getEtcMemo());

        pcRoomRepository.save(addData);
    }

    public List<PcRoomItem> getPcRoom() {
        List<PcRoom> originList = pcRoomRepository.findAll();

        List<PcRoomItem> result = new LinkedList<>();

        for (PcRoom pcRoom : originList) {
            PcRoomItem addItem = new PcRoomItem();
            addItem.setId(pcRoom.getId());
            addItem.setDateCreate(pcRoom.getDateCreate());
            addItem.setComputerNo(pcRoom.getComputerNo());
            addItem.setUpdate(pcRoom.getUpdate());
            addItem.setFixType(pcRoom.getFixType().getName());
            addItem.setPcCheck(pcRoom.getPcCheck().getPcStatus());

            result.add(addItem);

        }

        return result;
    }

    public PcRoomResponse getPcRoom(long id) {
        PcRoom originData = pcRoomRepository.findById(id).orElseThrow();

        PcRoomResponse response = new PcRoomResponse();
        response.setId(originData.getId());
        response.setDateCreate(originData.getDateCreate());
        response.setComputerNo(originData.getComputerNo());
        response.setUpdate(originData.getUpdate());
        response.setFixTypeName(originData.getFixType().getName());

        response.setPcCheckName(originData.getPcCheck().getPcStatusCheck() ? "점검 완료" : "점검 안됨");

        return response;
    }
}
